Name	Amount
Butter(unsalted)	1.5 sticks / 12 tbsp
Cilantro	1/2 bunch
Cranberryjuice	1 bottle
Dijonmustard 	1 bottle
Dressing	2 bottles
FujiApples	2 apples
Greenonions 	10 stalks
Honey	0
Ketchup 	1 bottle
Kewpiemayonnaise 	10 fl oz
Largebrowneggs	6 eggs
Lemon	1
Lime	1
Peanutbutter 	1 jar
Pickles	1 jar
Raspberryjam	1 jar
Soju	1 bottle
Springmix	1 bag
Sriracha 	0 bottle
Strawberryjam	1 jar
YerbaMate	2 cans
