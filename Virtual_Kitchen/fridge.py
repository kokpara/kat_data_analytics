#welcome User
userName = input('Please enter your name: ')
welcomeMessage = f"Welcome to the fridge inventory, {userName}!"
lenWCMsg = len(welcomeMessage)
print("*"*lenWCMsg)
print(welcomeMessage)
print("Let's see what's available today...")

#read data from text file
my_file=open("available_fridge_items.txt")

file_str = my_file.read()
print(file_str)

file_line = my_file.readline()
itemsAvailable = my_file.readlines()
print(itemsAvailable)
my_file.close()

#fetch an item from the list
for item in itemsAvailable:
    print(item)

#fetch an item from the list
itemAvailableDict = {}
for item in itemsAvailable:
    item_name = item.split()[0]
    item_amount = item.split()[1]
    print(f"{item_name}: {item_amount}")
    itemAvailableDict.update({item_name: item_amount})
    #itemAvailableDict.update({item_name: float(item_amount)})
    print(itemAvailableDict)
print("*"*lenWCMsg)

#prompt user to add items; use a while loop to continue until the user says no
updateFridge = input("Do you want to update the fridge inventory? (yes/no): ")
while updateFridge.lower() == "yes":
    add_new_name = input('Enter the name of the new item:')
    #print(add_new_name)
    if add_new_name not in itemAvailableDict:
        add_new_amount = input('Enter the amount of the new item:')
        #print(add_new_amount)
        #itemAvailableDict[add_new_name] = add_new_amount
        itemAvailableDict.update({add_new_name: add_new_amount})
        print("Great, your update is recorded!")
        updateFridge = input("Do you want to add other items into the fridge inventory? (yes/no): ")
        if updateFridge.lower() == "no":
            print("Here's the updated inventory: ", itemAvailableDict)
        else:
            pass
    else:
        print('This item is already in the inventory!')
        updateFridge = input("Do you want to add other items into the fridge inventory? (yes/no): ")
        if updateFridge.lower() == "no":
            print("Here's the updated inventory: ", itemAvailableDict)
        else:
            pass
else:
    print('See you next time!')

#import pandas as pd
#df_format = pd.read_fwf('available_fridge_items.txt')
#df_format.to_csv('available_fridge_items.csv')
